﻿
using System;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using XieAuth.App;
using XieAuth.App.Interface;
using XieAuth.App.Requests;

namespace XieAuth.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccessObjsController : ControllerBase
    {
        private readonly RevelanceManagerApp _app;
        private readonly IAuth _authUtil;
        public AccessObjsController(IAuth authUtil, RevelanceManagerApp app) 
        {
            _app = app;
            _authUtil = authUtil;
        }

        /// <summary>
        /// 比如给用户分配资源，那么firstId就是用户ID，secIds就是资源ID列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public DtoResult Assign(AssignReq request)
        {
            var result = new DtoResult();
            try
            {
                _app.Assign(request);
            }
            catch (Exception ex)
            {
                  result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }
        [HttpPost]
        public DtoResult UnAssign(AssignReq request)
        {
            var result = new DtoResult();
            try
            {
                _app.UnAssign(request.type, request.firstId, request.secIds);
            }
            catch (Exception ex)
            {
                  result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }
        
        /// <summary>
        /// 角色分配数据字段权限
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public DtoResult AssignDataProperty(AssignDataReq request)
        {
            var result = new DtoResult();
            try
            {
                _app.AssignData(request);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }
        /// <summary>
        /// 取消角色的数据字段权限
        /// <para>如果Properties为空，则把角色的某一个模块权限全部删除</para>
        /// <para>如果moduleId为空，直接把角色的所有授权删除</para>
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public DtoResult UnAssignDataProperty(AssignDataReq request)
        {
            var result = new DtoResult();
            try
            {
                _app.UnAssignData(request);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }
    }
}