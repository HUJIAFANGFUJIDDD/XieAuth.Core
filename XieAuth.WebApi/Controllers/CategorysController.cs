﻿using System;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using XieAuth.App;
using XieAuth.App.Requests;
using XieAuth.App.Responses;
using XieAuth.Repository.Domain;

namespace XieAuth.WebApi.Controllers
{
    /// <summary>
    /// Category操作
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CategorysController : ControllerBase
    {
        private readonly CategoryApp _app;
        
        //获取详情
        [HttpGet]
        public DtoResult<Category> Get(string id)
        {
            var result = new DtoResult<Category>();
            try
            {
                result.Result = _app.Get(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //添加
       [HttpPost]
        public DtoResult Add(Category obj)
        {
            var result = new DtoResult();
            try
            {
                _app.Add(obj);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //修改
       [HttpPost]
        public DtoResult Update(Category obj)
        {
            var result = new DtoResult();
            try
            {
                _app.Update(obj);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public TableData Load([FromQuery]QueryCategoryListReq request)
        {
            return _app.Load(request);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
       [HttpPost]
        public DtoResult Delete([FromBody]string[] ids)
        {
            var result = new DtoResult();
            try
            {
                _app.Delete(ids);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        public CategorysController(CategoryApp app) 
        {
            _app = app;
        }
    }
}
