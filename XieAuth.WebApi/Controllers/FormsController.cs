﻿using System;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using XieAuth.App;
using XieAuth.App.Requests;
using XieAuth.App.Responses;
using XieAuth.Repository.Domain;

namespace XieAuth.WebApi.Controllers
{
    /// <summary>
    /// 表单操作
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FormsController : ControllerBase
    {
        private readonly FormApp _app;

        [HttpGet]
        public DtoResult<FormResp> Get(string id)
        {
            var result = new DtoResult<FormResp>();
            try
            {
                result.Result = _app.FindSingle(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //添加或修改
       [HttpPost]
        public DtoResult Add(Form obj)
        {
            var result = new DtoResult();
            try
            {
                _app.Add(obj);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //添加或修改
       [HttpPost]
        public DtoResult Update(Form obj)
        {
            var result = new DtoResult();
            try
            {
                _app.Update(obj);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public TableData Load([FromQuery]QueryFormListReq request)
        {
            return _app.Load(request);
        }

       [HttpPost]
        public DtoResult Delete([FromBody]string[] ids)
        {
            var result = new DtoResult();
            try
            {
                _app.Delete(ids);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        public FormsController(FormApp app) 
        {
            _app = app;
        }
    }
}