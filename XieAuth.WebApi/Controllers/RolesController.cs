﻿using System;
using System.Collections.Generic;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;
using XieAuth.App;
using XieAuth.App.Helpers;
using XieAuth.App.Requests;
using XieAuth.App.Responses;
using XieAuth.Repository.Domain;

namespace XieAuth.WebApi.Controllers
{
    /// <summary>
    /// 表单操作
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly RoleApp _app;
        private readonly RevelanceManagerApp _revelanceManagerApp;
        public RolesController(RevelanceManagerApp revelanceManagerApp, RoleApp app) 
        {
            _revelanceManagerApp = revelanceManagerApp;
            _app = app;
        }

        [HttpGet]
        public DtoResult<Role> Get(string id)
        {
            var result = new DtoResult<Role>();
            try
            {
                result.Result = _app.Get(id);
            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //添加或修改
       [HttpPost]
        public DtoResult<RoleResp> Add(RoleResp obj)
        {
            var result = new DtoResult<RoleResp>();
            try
            {
                _app.Add(obj);
                result.Result = obj;

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        //添加或修改
       [HttpPost]
        public DtoResult Update(RoleResp obj)
        {
            var result = new DtoResult();
            try
            {
                _app.Update(obj);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 加载列表
        /// </summary>
        [HttpGet]
        public TableData Load([FromQuery]QueryRoleListReq request)
        {
            return _app.Load(request);
        }

        /// <summary>
        /// 加载用户的角色
        /// </summary>
        [HttpGet]
        public DtoResult<List<string>> LoadForUser(string userId)
        {
            var result = new DtoResult<List<string>>();
            try
            {
                result.Result = _revelanceManagerApp.Get(DefineHelper.USERROLE, true, userId);
            }
            catch (Exception e)
            {
                result.Code = 500;
                result.Message = e.InnerException?.Message ?? e.Message;
            }

            return result;
        }

        [HttpPost]
        public DtoResult Delete([FromBody]string[] ids)
        {
            var result = new DtoResult();
            try
            {
                _app.Delete(ids);

            }
            catch (Exception ex)
            {
                result.Code = 500;
                result.Message = ex.InnerException?.Message ?? ex.Message;
            }

            return result;
        }
    }
}