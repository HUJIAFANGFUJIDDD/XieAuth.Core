﻿using Microsoft.AspNetCore.Mvc;
using XieAuth.App;
using XieAuth.App.Requests;
using XieAuth.App.Responses;

namespace XieAuth.WebApi.Controllers
{
    /// <summary>
    /// 应用列表
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ApplicationsController : ControllerBase
    {
        private readonly ManagerApp _app;

        public ApplicationsController(ManagerApp app) 
        {
            _app = app;
        }
        [HttpGet]
        public TableData Load([FromQuery]QueryAppListReq request)
        {
            var applications = _app.GetList(request);
            return new TableData
            {
                data = applications,
                count = applications.Count
            };
        }

    }
}