﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using Autofac.Extensions.DependencyInjection;
using IdentityServer4.AccessTokenValidation;
using Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using XieAuth.App;
using XieAuth.Repository;
using XieAuth.WebApi.Middlewares;
using XieAuth.WebApi.Loggers;
using XieAuth.WebApi.Filters;
using Swashbuckle.AspNetCore.Swagger;
using XieAuth.App.Helpers;

namespace XieAuth.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IOptions<AppSetting> appConfiguration)
        {
            Configuration = configuration;
            _appConfiguration = appConfiguration;
        }

        public IConfiguration Configuration { get; }
        public IOptions<AppSetting> _appConfiguration;

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var identityServer = ((ConfigurationSection)Configuration.GetSection("AppSetting:IdentityServerUrl")).Value;
            if (!string.IsNullOrEmpty(identityServer))
            {
                services.AddAuthorization();

                services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        options.Authority = identityServer;
                        options.RequireHttpsMetadata = false;  // 指定是否为HTTPS
                        options.Audience = "openauthapi";
                    });
            }
          

            services.AddSwaggerGen(option =>
            {
                option.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Version = "v1",
                    Title = " XieAuth.WebApi",
                    Description = "by yubaolee"
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                option.IncludeXmlComments(xmlPath);
                option.OperationFilter<GlobalHttpHeaderOperationFilter>(); // 添加httpHeader参数

                if (!string.IsNullOrEmpty(identityServer))
                {
                    //接入identityserver
                    option.AddSecurityDefinition("oauth2", new OAuth2Scheme
                    {
                        Flow = "implicit", // 只需通过浏览器获取令牌（适用于swagger）
                        AuthorizationUrl = $"{identityServer}/connect/authorize",//获取登录授权接口
                        Scopes = new Dictionary<string, string> {
                            { "openauthapi", "同意openauth.webapi 的访问权限" }//指定客户端请求的api作用域。 如果为空，则客户端无法访问
                        }
                    });
                    option.OperationFilter<AuthDtoResultsOperationFilter>();
                }

                
            });
            services.Configure<AppSetting>(Configuration.GetSection("AppSetting"));
            services.AddMvc(config =>
            {
                config.Filters.Add<ApiAuthFilter>();
            })
            .AddControllersAsServices().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
            })
            .ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {

                    Dictionary<string, string> dict = new Dictionary<string, string>();
                    foreach (var state in context.ModelState)
                    {
                        dict.Add(state.Key, string.Join(" | ", state.Value.Errors.Select(e => e.ErrorMessage)));
                    }
                    var resultDto = new DtoResult<Dictionary<string, string>>()
                    {
                        Code = -1,
                        Message = "输入参数有误",
                        Result = dict
                    };
                    return new JsonResult(resultDto);
                };
            });
            services.AddMemoryCache();
            services.AddCors();
            //在startup里面只能通过这种方式获取到appsettings里面的值，不能用IOptions😰
            var dbType = ((ConfigurationSection)Configuration.GetSection("AppSetting:DbType")).Value;
            if (dbType == DefineHelper.DBTYPE_SQLSERVER)
            {
                services.AddDbContext<XieAuthDBContext>(options => 
                {
                    //var loggerFactory = new LoggerFactory();
                    //loggerFactory.AddProvider(new EFLoggerProvider());
                    options.UseSqlServer(Configuration.GetConnectionString("XieAuthDBContext")).UseLoggerFactory(EFLoggerFactory);
                });
            }
            else  //mysql
            {
                services.AddDbContext<XieAuthDBContext>(options =>
                {
                    //var loggerFactory = new LoggerFactory();
                    //loggerFactory.AddProvider(new EFLoggerProvider());
                    options.UseMySql(Configuration.GetConnectionString("XieAuthDBContext")).UseLoggerFactory(EFLoggerFactory);
                });
            }


            return new AutofacServiceProvider(AutofacHelper.InitAutofac(services));
        }

        public static readonly LoggerFactory EFLoggerFactory = new LoggerFactory(new[] { new EFLoggerProvider() });


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseApiExceptionJsonHandler();

            //可以访问根目录下面的静态文件
            var staticfile = new StaticFileOptions {FileProvider = new PhysicalFileProvider(AppContext.BaseDirectory) };
            app.UseStaticFiles(staticfile);

            //todo:测试可以允许任意跨域，正式环境要加权限
            app.UseCors(builder => builder.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());


            app.UseAuthentication();
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");
                c.DocExpansion(DocExpansion.None);
                c.OAuthClientId("XieAuth.WebApi");  //oauth客户端名称
                c.OAuthAppName("客户端为XieAuth.WebApi"); // 描述
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "api/{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
