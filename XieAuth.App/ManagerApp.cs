﻿using System;
using System.Collections.Generic;
using System.Linq;
using XieAuth.App.Requests;
using XieAuth.App.Responses;
using XieAuth.Repository.Domain;
using XieAuth.Repository.Interface;

namespace XieAuth.App
{
    /// <summary>
    /// 分类管理
    /// </summary>
    public class ManagerApp : BaseApp<Application>
    {
        public void Add(Application application)
        {
            if (string.IsNullOrEmpty(application.Id))
            {
                application.Id = Guid.NewGuid().ToString();
            }
            Repository.Add(application);
        }

        public void Update(Application application)
        {
            Repository.Update(application);
        }


        public List<Application> GetList(QueryAppListReq request)
        {
            var applications =  UnitWork.Find<Application>(null) ;
           
            return applications.ToList();
        }

        public ManagerApp(IUnitWork unitWork, IRepository<Application> repository) : base(unitWork, repository)
        {
        }
    }
}