﻿namespace XieAuth.App.Requests
{
    public class QueryUserListReq : PageReq
    {
        public string orgId { get; set; }
    }
}
