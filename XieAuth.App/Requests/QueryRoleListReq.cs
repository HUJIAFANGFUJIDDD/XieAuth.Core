﻿namespace XieAuth.App.Requests
{
    public class QueryRoleListReq : PageReq
    {
        public string orgId { get; set; }
    }
}
