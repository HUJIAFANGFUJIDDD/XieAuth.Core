﻿namespace XieAuth.App.Requests
{
    public class QueryFlowSchemeListReq : PageReq
    {
        public string orgId { get; set; }
    }
}
