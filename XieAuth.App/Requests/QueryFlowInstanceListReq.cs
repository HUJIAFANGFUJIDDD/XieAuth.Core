﻿namespace XieAuth.App.Requests
{
    public class QueryFlowInstanceListReq : PageReq
    {
        public string type { get; set; }
    }
}
