﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XieAuth.App.Requests
{
    public class ChangePasswordReq
    {
        public string Account { get; set; }
        public string Password { get; set; }
    }
}
