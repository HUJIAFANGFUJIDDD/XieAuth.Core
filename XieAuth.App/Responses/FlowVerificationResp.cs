﻿using XieAuth.App.Helpers;
using XieAuth.Repository.Domain;

namespace XieAuth.App.Responses
{
    public class FlowVerificationResp :FlowInstance
    {
        /// <summary>
        /// 预览表单数据
        /// </summary>
        /// <value>The FRM data HTML.</value>
        public string FrmPreviewHtml => FormHelper.Preview(FrmContentData, FrmContentParse, FrmData);
    }
}
