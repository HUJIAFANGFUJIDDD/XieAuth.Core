using Infrastructure;

namespace XieAuth.App.Modules.SSO
{
    public class LoginResult :DtoResult<string>
    {
        public string ReturnUrl;
        public string Token;
    }
}