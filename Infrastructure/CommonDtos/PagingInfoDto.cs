﻿using System;

namespace Infrastructure.CommonDtos
{
    public class PagingInfoDto
    {
        public int TotalCount { get; set; }

        public int CountPerPage { get; set; }

        public int CurrentPage { get; set; }

        public int TotalPages => (int)Math.Ceiling((decimal)TotalCount / CountPerPage);
    }
}