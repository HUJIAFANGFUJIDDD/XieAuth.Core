﻿using System;

namespace Infrastructure.CommonDtos
{
    public class SearchInfoDto
    {
        public string Key { get; set; }

        public string Flag { get; set; }

        public int State { get; set; } = -1;

        public DateTime? BeginDt { get; set; }

        public DateTime? EndDt { get; set; }
    }
}