﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure
{
    public class DtoResultException:Exception
    {
        public DtoResult result { get; set; }
        public DtoResultException(DtoResult r)
        {
            result = r;
        }
    }
}
