﻿
using System;

namespace Infrastructure.Exceptions
{
    public class CommonException : Exception
    {
        public CommonException(string message, int code)
            : base(message)
        {
            this.Code = code;
        }

        public int Code { get; }

    }
}
