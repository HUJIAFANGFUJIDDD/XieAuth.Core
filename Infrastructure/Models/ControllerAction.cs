using System;
using System.Collections.Generic;

namespace Infrastructure.Models
{
    public class ControllerData
    {
        public string AreaName { get; set; }
        public string ControllerName { get; set; }
        public string Namespace { get; set; }
        public string Controller { get; set; }
        public string ModuleName { get; set; }
        public IEnumerable<ActionData> Actions { get; set; }
    }
    public class ActionData
    {
        public string Name { get; set; }
        public string HttpMethods { get; set; }
        public int ParameterCount { get; set; }
        public dynamic Parameters { get; set; }   
           
    }
}