﻿namespace Infrastructure
{
    /// <summary>
    ///  默认为200，操作成功
    /// </summary>
    public class DtoResult
    {
        /// <summary>
        /// 操作消息【当Status不为 200时，显示详细的错误信息】
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 操作状态码，200为正常
        /// </summary>
        public int Code { get; set; }

        /// <summary>
        ///  默认为200，操作成功
        /// </summary>
        public DtoResult()
        {
            Code = 200;
            Message = "操作成功";
        }

        public static DtoResult ErrorRequest(string msg= "请求参数有问题")
        {
            return new DtoResult() { Code = 400,Message = msg};
        }
        public static DtoResult ErrorServer(string msg = "服务器出现故障")
        {
            return new DtoResult() { Code = 500, Message =msg };
        }
    }


    /// <summary>
    /// 默认为200，操作成功
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DtoResult<T> : DtoResult
    {
        /// <summary>
        /// 回传的结果
        /// </summary>
        public T Result { get; set; }
    }
}
