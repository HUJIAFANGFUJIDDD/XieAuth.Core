﻿using System;
using Infrastructure;
using Infrastructure.Helpers;
using Microsoft.AspNetCore.Mvc;
using XieAuth.App;
using XieAuth.App.Interface;
using XieAuth.App.Requests;
using XieAuth.App.Responses;
using XieAuth.Repository.Domain;

namespace XieAuth.Mvc.Controllers
{
    public class CategoriesController : BaseController
    {
        private readonly CategoryApp _app;
        public CategoriesController(IAuth authUtil, CategoryApp app) : base(authUtil)
        {
            _app = app;
        }

        //
        // GET: /UserManager/
        public ActionResult Index()
        {
            return View();
        }

        public string All([FromQuery]QueryCategoryListReq request)
        {
            TableData data = new TableData();
            data = _app.Load(request);
            return JsonHelper.Instance.Serialize(data);
        }

       [HttpPost]
        public string Delete(string[] ids)
        {
            DtoResult resp = new DtoResult();
            try
            {
                _app.Delete(ids);
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }
            return JsonHelper.Instance.Serialize(resp);
        }

       [HttpPost]
        public string Add(Category obj)
        {
            DtoResult resp = new DtoResult();
            try
            {
                _app.Add(obj);
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }
            return JsonHelper.Instance.Serialize(resp);
        }

       [HttpPost]
        public string Update(Category obj)
        {
            DtoResult resp = new DtoResult();
            try
            {
                _app.Update(obj);
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }
            return JsonHelper.Instance.Serialize(resp);
        }

        //所有得分类类型
        public string AllTypes()
        {
            var data = _app.AllTypes();
            return JsonHelper.Instance.Serialize(data);
        }

    }
}