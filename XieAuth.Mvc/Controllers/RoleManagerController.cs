﻿using System;
using System.Collections.Generic;
using Infrastructure;
using Infrastructure.Helpers;
using Microsoft.AspNetCore.Mvc;
using XieAuth.App;
using XieAuth.App.Helpers;
using XieAuth.App.Interface;
using XieAuth.App.Requests;
using XieAuth.App.Responses;

namespace XieAuth.Mvc.Controllers
{
    public class RoleManagerController : BaseController
    {
        private readonly RoleApp _app;
        private readonly RevelanceManagerApp _revelanceManagerApp;
        public RoleManagerController(IAuth authUtil, RevelanceManagerApp revelanceManagerApp, RoleApp app) : base(authUtil)
        {
            _revelanceManagerApp = revelanceManagerApp;
            _app = app;
        }
        //
        // GET: /UserManager/
       
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Assign()
        {
            return View();
        }

        //添加或修改组织
       [HttpPost]
        public string Add(RoleResp obj)
        {
            try
            {
                _app.Add(obj);

            }
            catch (Exception ex)
            {
                Result.Code = 500;
                Result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return JsonHelper.Instance.Serialize(Result);
        }

        //添加或修改组织
       [HttpPost]
        public string Update(RoleResp obj)
        {
            try
            {
                _app.Update(obj);

            }
            catch (Exception ex)
            {
                Result.Code = 500;
                Result.Message = ex.InnerException?.Message ?? ex.Message;
            }
            return JsonHelper.Instance.Serialize(Result);
        }

        /// <summary>
        /// 加载用户的角色
        /// </summary>
        public string LoadForUser(string userId)
        {
            try
            {
                var result = new DtoResult<List<string>>
                {
                    Result = _revelanceManagerApp.Get(DefineHelper.USERROLE, true, userId)
                };
                return JsonHelper.Instance.Serialize(result);
            }
            catch (Exception e)
            {
                Result.Code = 500;
                Result.Message = e.InnerException?.Message ?? e.Message;
            }

            return JsonHelper.Instance.Serialize(Result);
        }

        /// <summary>
        /// 加载组织下面的所有用户
        /// </summary>
        public string Load([FromQuery]QueryRoleListReq request)
        {
            return JsonHelper.Instance.Serialize(_app.Load(request));
        }

       [HttpPost]
        public string Delete(string[] ids)
        {
            try
            {
                _app.Delete(ids);
            }
            catch (Exception e)
            {
                Result.Code = 500;
                Result.Message = e.InnerException?.Message ?? e.Message;
            }

            return JsonHelper.Instance.Serialize(Result);
        }

    }
}