﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure;
using Infrastructure.Helpers;
using Microsoft.AspNetCore.Mvc;
using XieAuth.App;
using XieAuth.App.Interface;
using XieAuth.App.Requests;
using XieAuth.Repository.Domain;

namespace XieAuth.Mvc.Controllers
{
    public class ResourcesController : BaseController
    {
        private readonly ResourceApp _app;

        public ResourcesController(IAuth authUtil, ResourceApp app) : base(authUtil)
        {
            _app = app;
        }
        //
        // GET: /UserManager/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Assign()
        {
            return View();
        }

        /// <summary>
        /// 加载角色资源
        /// </summary>
        /// <param name="appId">应用ID</param>
        /// <param name="firstId">角色ID</param>
        public string LoadForRole(string appId, string firstId)
        {
            try
            {
                var result = new DtoResult<List<string>>
                {
                    Result = _app.LoadForRole(appId, firstId).Select(u => u.Id).ToList()
                };
                return JsonHelper.Instance.Serialize(result);
            }
            catch (Exception e)
            {
                Result.Code = 500;
                Result.Message = e.InnerException?.Message ?? e.Message;
            }

            return JsonHelper.Instance.Serialize(Result);
        }


        public string Load([FromQuery]QueryResourcesReq request)
        {
            return JsonHelper.Instance.Serialize(_app.Load(request));
        }

       [HttpPost]
        public string Delete(string[] ids)
        {
            DtoResult resp = new DtoResult();
            try
            {
                _app.Delete(ids);
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }
            return JsonHelper.Instance.Serialize(resp);
        }

       [HttpPost]
        public string Add(Resource obj)
        {
            DtoResult resp = new DtoResult();
            try
            {
                _app.Add(obj);
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }
            return JsonHelper.Instance.Serialize(resp);
        }

       [HttpPost]
        public string Update(Resource obj)
        {
            DtoResult resp = new DtoResult();
            try
            {
                _app.Update(obj);
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }
            return JsonHelper.Instance.Serialize(resp);
        }

    }
}