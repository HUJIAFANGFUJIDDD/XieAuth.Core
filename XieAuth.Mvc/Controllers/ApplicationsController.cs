﻿using System;
using System.Collections.Generic;
using Infrastructure;
using Infrastructure.Helpers;
using Microsoft.AspNetCore.Mvc;
using XieAuth.App;
using XieAuth.App.Interface;
using XieAuth.App.Requests;
using XieAuth.Repository.Domain;

namespace XieAuth.Mvc.Controllers
{
    public class ApplicationsController : BaseController
    {
        private readonly ManagerApp _app;


        public string GetList([FromQuery]QueryAppListReq request)
        {
            var resp = new DtoResult<List<Application>>();
            try
            {
                resp.Result = _app.GetList(request);
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }
            return JsonHelper.Instance.Serialize(resp);
        }

       [HttpPost]
        public string Delete(string[] ids)
        {
            DtoResult resp = new DtoResult();
            try
            {
                _app.Delete(ids);
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }
            return JsonHelper.Instance.Serialize(resp);
        }

       [HttpPost]
        public string Add(Application obj)
        {
            DtoResult resp = new DtoResult();
            try
            {
                _app.Add(obj);
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }
            return JsonHelper.Instance.Serialize(resp);
        }

       [HttpPost]
        public string Update(Application obj)
        {
            DtoResult resp = new DtoResult();
            try
            {
                _app.Update(obj);
            }
            catch (Exception e)
            {
                resp.Code = 500;
                resp.Message = e.Message;
            }
            return JsonHelper.Instance.Serialize(resp);
        }


        public ApplicationsController(IAuth authUtil, ManagerApp app) : base(authUtil)
        {
            _app = app;
        }
    }
}