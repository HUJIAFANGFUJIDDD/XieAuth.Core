﻿using System;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using XieAuth.App;
using XieAuth.App.Helpers;
using XieAuth.Mvc.Loggers;
using XieAuth.Mvc.Filters;
using XieAuth.Repository;
using XieAuth.Mvc.Middlewares;

namespace XieAuth.Mvc
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var identityServer = ((ConfigurationSection)Configuration.GetSection("AppSetting:IdentityServerUrl")).Value;
            if (!string.IsNullOrEmpty(identityServer))
            {
                System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

                services.AddAuthentication(options =>
                {
                    options.DefaultScheme = "Cookies";
                    options.DefaultChallengeScheme = "oidc";
                })
                .AddCookie("Cookies")
                .AddOpenIdConnect("oidc", options =>
                {
                    options.Authority = identityServer;
                    options.RequireHttpsMetadata = false;

                    options.ClientId = "XieAuth.Mvc";
                    options.SaveTokens = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        NameClaimType = "name",
                        RoleClaimType = "role",
                    };
                });
            }

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                //关闭GDPR规范
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc(option =>
            {
                option.Filters.Add<MvcRequestLogFilter>();
                option.Filters.Add<MvcAuthFilter>();
                option.ModelBinderProviders.Insert(0, new JsonBinderProvider());
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddMemoryCache();
            services.AddOptions();

            services.AddRouting(options => options.LowercaseUrls = false);

            //映射配置文件
            services.Configure<AppSetting>(Configuration.GetSection("AppSetting"));

            //在startup里面只能通过这种方式获取到appsettings里面的值，不能用IOptions😰
            var dbType = ((ConfigurationSection) Configuration.GetSection("AppSetting:DbType")).Value;
            if (dbType == DefineHelper.DBTYPE_SQLSERVER)
            {
                services.AddDbContext<XieAuthDBContext>(options =>
                {
                    //var loggerFactory = new LoggerFactory();
                    //loggerFactory.AddProvider(new EFLoggerProvider());
                    options.UseSqlServer(Configuration.GetConnectionString("XieAuthDBContext")).UseLoggerFactory(EFLoggerFactory);
                });
            }
            else  //mysql
            {
                services.AddDbContext<XieAuthDBContext>(options =>
                {
                    //var loggerFactory = new LoggerFactory(new[] { new EFLoggerProvider() });
                    //loggerFactory.AddProvider(new EFLoggerProvider());
                    options.UseMySql(Configuration.GetConnectionString("XieAuthDBContext")).UseLoggerFactory(EFLoggerFactory);
                });
            }
            //使用AutoFac进行注入
            return new AutofacServiceProvider(AutofacHelper.InitAutofac(services));
        }

        public static readonly LoggerFactory EFLoggerFactory = new LoggerFactory(new[] { new EFLoggerProvider() });

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseMvcExceptionJsonHandler();

            app.UseAuthentication();

            app.UseStaticFiles();
            app.UseCookiePolicy();  

            app.UseMvcWithDefaultRoute();
            
        }
    }
}
